#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 15 15:00:00 2022

@author: william
"""

# We need a way of graphing a bunch of eigenvalues together. Quick and dirty,
# nothing too fancy.
import json
import sys

import numpy as np
from matplotlib import pyplot as plt
from matplotlib import style, ticker

style.use("posterstyle")

values_to_plot = {"1s²":(["out_data/s-poster-graph1a.json",
                          "out_data/s-poster-graph2a.json",
                          "out_data/s-poster-graph3a.json",
                          "out_data/s-poster-graph4a.json",
                          "out_data/s-poster-graph5a.json",
                          "out_data/s-poster-graph6a.json",
                          "out_data/s-poster-graph7a.json",
                          "out_data/s-poster-graph8a.json",
                          "out_data/s-poster-graph9a.json",
                          "out_data/s-poster-graph10a.json",
                          "out_data/s-poster-graph11a.json",
                          "out_data/s-poster-graph12a.json",], (0,1,2), 0, 0),
                  "1s2s":([#"out_data/s-poster-graph1b.json",
                           "out_data/s-poster-graph2b.json",
                           "out_data/s-poster-graph3b.json",
                           "out_data/s-poster-graph4b.json",
                           "out_data/s-poster-graph5b.json",
                           "out_data/s-poster-graph6b.json",
                           "out_data/s-poster-graph7b.json",
                           "out_data/s-poster-graph8b.json",
                           "out_data/s-poster-graph9b.json",
                           "out_data/s-poster-graph10b.json",
                           "out_data/s-poster-graph11b.json",
                           #"out_data/s-poster-graph12b.json",
                           ], (1,), 1, 0),
                  "1s3s":([#"out_data/s-poster-graph1c.json",
                           #"out_data/s-poster-graph2c.json",
                           "out_data/s-poster-graph3c.json",
                           "out_data/s-poster-graph4c.json",
                           "out_data/s-poster-graph5c.json",
                           "out_data/s-poster-graph6c.json",
                           "out_data/s-poster-graph7c.json",
                           "out_data/s-poster-graph8c.json",
                           "out_data/s-poster-graph9c.json",
                           "out_data/s-poster-graph10c.json",
                           "out_data/s-poster-graph11c.json",
                           #"out_data/s-poster-graph12c.json",
                           ], (2,), 2, 0)}

fig = plt.figure(figsize=(6.4,6.4))

ax = fig.add_subplot(111)

p_cycle = plt.rcParams["axes.prop_cycle"]

# Now, we need to assemble the converging eigenvalue series from the list
# of dictionaries:
legend_artists = [] 
for key, series in values_to_plot.items():
    x_vals = np.zeros(len(series[0]))
    y_vals = np.full((len(series[0]), len(series[1])), np.NaN)
    for n, filename in enumerate(series[0]):
        try:
            dat_file = open(filename, "r")
            with dat_file:
                dat_json = json.load(dat_file)
                xval = len(dat_json["basis"])
                yvals_raw = np.array(dat_json["data"]["ritz_upper"]["extra"]["eigenenergies"])
        except (OSError, json.JSONDecodeError) as err:
            print(err, file=sys.stderr)
            xval = np.NaN
            yvals_raw = np.array([np.NaN]*len(series[1]))
        x_vals[n] = xval
        idx = np.array(series[1])
        invalid_idx = idx > yvals_raw.shape[0]-1
        y_vals[n] = yvals_raw.take(idx.clip(0, yvals_raw.shape[0]-1))
        y_vals[n][np.where(invalid_idx)] = np.NaN
    # Then, we plot them:
    y_vals_plt = np.block([y_vals, np.full((y_vals.shape[0], 3-y_vals.shape[1]), np.NaN)])
    artists = ax.plot(x_vals, y_vals_plt, label=key)
    #for n, artist in filter(lambda x: x[0] != series[3], 
    #                        zip(series[1], artists)):
    #    artist.set(**(tuple(islice(p_cycle, series[2]+3, series[2]+4)))[0])
    try:
        legend_artist = artists[series[3]]
    except IndexError:
        legend_artist = artists[0]
    legend_artists.append(legend_artist)

# Error dividing lines
ax.axvline(11.5, ymin=0, ymax=0.6, lw=1.0, c="r", ls="--", zorder=-1)
ax.axvline(8.5,  ymin=0.2375, ymax=0.3375, lw=1.0, c="b", ls="--", zorder=-1)
ax.axvline(7.5,  ymin=0.3625, ymax=0.4625, lw=1.0, c="g", ls="--", zorder=-1)

ax.set_xlabel("Number of basis states")
ax.set_ylabel("E / Hartree energy")
ax.legend(legend_artists, values_to_plot.keys(), title="Optimised level")
ax.set_title("Eigenenergy convergence")
ax.set_xlim((0,14))
ax.set_ylim((-3.0, -0.0))   

# We'll also need to put in the reference lines. Seems to be converging to 
# the ¹S lines, as I would expect.

reference_energies = [(-2.90338588043954, "1s²"), 
                      (-2.14577011707376, "1s2s"), 
                      (-2.06107974037633, "1s3s"),
                      (-1.99981599893257, "Ion")]

ref_ax = ax.twinx()
for energy, label in reference_energies:
    ref_ax.axhline(energy, lw=1.0, c="k", ls="--", zorder=-1)

level_ticker = ticker.FixedLocator([energy for energy, label in reference_energies])
level_formatter = ticker.FixedFormatter([label for energy, label in reference_energies])
ref_ax.yaxis.set_major_locator(level_ticker)
ref_ax.yaxis.set_major_formatter(level_formatter)
ref_ax.set_ylim(ax.get_ylim())
ref_ax.set_ylabel("Reference energies")


#plt.show()
plt.savefig("convergence.png", dpi=300)