#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 12 16:33:52 2021

@author: willow
"""

import argparse as ap
import json
import textwrap
import sys
import time

import scipy.optimize
import numpy as np

#
# Computing Project Goal:
#   * Get lower bounds for a variety of excited states of a Helium atom.
#       * Implement variance calculations (properly).
#           * Implement way of finding the resulting function of the Hamiltonian
#           * Implement numerical integration of basis states.
#               * Monte Carlo integration package vegas installed.
#                 We just need the integration domain.
#       * Switch to a new (two parameter) basis set from Drake. 
#
#   * 
#

#
# Unsolved problems:
#   * "Precision loss"
#   * Divisions by zero
#   * Unsound variances
#

# Bringing in our suite for working with Hilbert spaces.
import tools
from tools import algebra, pyvq_util


func_choices_dict = pyvq_util.EnergyFunction.registry

# Getting version information. There are *definitely* better ways of doing this...
with open(".prog_info.json") as info_file:
    prog_info = json.load(info_file)
    version, _, g_commit = prog_info["description"].split("-")
    # ^ Gets the latest "git describe --tags" output from the JSON file.
    commit = g_commit[1:]  # Removes the "g" prefix from git describe.

# Putting the CLI together.
PROG_DOC = f"""\
PyVQM version {version}-{commit}

\
"""
#Optimised parameters and additional data can be saved in JSON format 
#"""
DESC_WRAPPED = [textwrap.wrap(para) for para in PROG_DOC.splitlines()]
PROGRAM_DESC = "\n".join("\n".join(line for line in para) 
                         for para in DESC_WRAPPED)

argparser = ap.ArgumentParser(description=PROGRAM_DESC,
                              formatter_class=ap.RawTextHelpFormatter)
# Argument for a adding a function to optimise over
argparser.add_argument("--function", "-f", action="append", 
                       choices=func_choices_dict.keys(),
                       dest="functions", default=[], 
                       help="Provides a function to optimise.",
                       required=True)

# Argument for adding a basis state
argparser.add_argument("--basis", "-b", action="append", type=int, nargs=3, 
                       metavar=("J", "K", "M"), 
                       help="Adds a basis state to optimise over.")

# Precision to display floats at. 
argparser.add_argument("--float-length", "-fl", type=int, default=6,
                       dest="floatlength",
                       help="Precision to display floating point values at.")

argparser.add_argument("--output", "-o", action="append",
                       help="Adds a file to save program results to, failing if file already exists.")
argparser.add_argument("--force-output", "-fo", action="append", dest="forceoutput",
                       help="Adds a file to save program results to, overwriting if file already exists.")

# Tells the program to shut it.
argparser.add_argument("--silent", "-s", action="count", default=0,
                       help="Suppresses output to stdout. Repeat twice to suppress stderr output.")

args = argparser.parse_args()

FL = args.floatlength

# Setting up basis states and dictionary of functions to optimise.
basis = [tuple(b) for b in args.basis]
basis_states = [tools.algebra.HyllerasState(basis, arr)
                for arr in np.identity(len(basis))]

# Handy thing to keep a bunch of arguments together.
context = {"basis": basis_states,
           "eigenvalue": 2}

optimise_funcs = {k:func_choices_dict[k](**context)
                  for k in args.functions}

# Optimise each function in optimise_funcs, store results.
results = {}
for func_id, func in optimise_funcs.items():
    try:
        start = time.time()
        out = scipy.optimize.minimize(func, np.array([1]*func.param_length), 
                                      args=(basis_states),
                                      bounds=((0, None),))
    except np.linalg.LinAlgError as err:
        if args.silent < 2:
            print(f"Due to an error ({err}), {func.__name__} cannot be minimised.",
                  file=sys.stderr)
        continue
    end = time.time()
    success = "Successful" if out.success else "Unsuccessful"
    if not args.silent:
        print(textwrap.dedent(f"""\
            Result for optimisation mode: {func_id}
                Result: {success} optimisation (SciPy: "{out.message}")
                Ground state energy found: {out.fun:.{FL}f} Eₕ ({out.fun*tools.util.E_H_EV:.{FL}f} eV)\
        """), sep="\n")
        print(f"Optimisation time: {end-start:.{FL}f} s")
    
    extra_data = func.extra_data(minimize_result=out, **context)
    # Putting the output data object together.
    result = {"minimize_result": out,
              "minimize_time": end-start,
              "extra": extra_data}
    results[func_id] = result

# Final summaries: target energies.
if not args.silent:
    if basis == [(0, 0, 0)]:
        result_000 = scipy.optimize.minimize(tools.util.analytical_energy_000,
                                             np.array([1]),
                                             bounds=((0, None),))
        energy_should_be = result_000.fun
        energy_should_be_EV = energy_should_be * tools.util.E_H_EV
        print(f"Expected minimum for basis [(0, 0, 0)]: {energy_should_be:.{FL}f} Eₕ ({energy_should_be_EV:.{FL}f} eV).")
        
    print(f"Energies should be ≥ -2.904 Eₕ ({-2.904*tools.util.E_H_EV:.{FL}f} eV).")


# Dumping the output results to the appropriate files.
results_pack = {"version": 1,
                "basis": basis,
                "data": results}
if args.output is not None:
    tools.pyvq_util.json_dump_to_files(args.output, results_pack)
if args.forceoutput is not None:
    tools.pyvq_util.json_dump_to_files(args.forceoutput,
                                       results_pack,
                                       mode="w")
