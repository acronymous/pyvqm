#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  4 15:23:42 2022

@author: william
"""

import numpy as np

from tools import util

levels = [("1s2s", 159_855.9743297),
          ("1s3s", 183_236.79170),
          ("1s4s", 190_940.226355),
          ("1s5s", 193_663.512095)]

for k, v in levels:
    print(k, v*200*np.pi/(util.R_0_SI))