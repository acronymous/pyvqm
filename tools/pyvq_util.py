#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 18 23:37:01 2021

@author: william

High-level utility functions for PyVQM
"""

from abc import ABCMeta, abstractmethod, abstractproperty
import functools
import itertools
import json
import operator as op
import sys

import numpy as np

from . import algebra, util

#
# So, the plan is to minimise the eigenvalues of a certain matrix.
# This matrix, P, is given by P=U*Y.T*H*Y*U, where:
#   * U is the diagonal inverse square root of N, the inner product matrix
#     of the basis states.
#   * Y is the matrix of eigenvectors of N.
#   * H is the Hamiltonian matrix

#
# First off, we need a way of generating this matrix.
#

def P(states, L):
    N_results = np.fromiter((algebra.I.sandwich(s1, s2, L)
                             for s1, s2 in itertools.product(states, states)),
                            dtype=np.float64)
    N_mat = N_results.reshape((len(states), len(states)))
    # ^ If anyone knows a better way of generating a square matrix from an
    #   iterable, let me know.
    
    evs, Y = np.linalg.eig(N_mat) # Eigenvalues and eigenvectors
    print(evs, L)
    
    U = np.diag(np.fromiter((1/np.sqrt(eigenvalue) for eigenvalue in evs),
                            dtype=np.float64))
    
    H_results = np.fromiter((algebra.H.sandwich(s1, s2, L) 
                             for s1, s2 in itertools.product(states, states)),
                            dtype=np.float64)
    H = H_results.reshape((len(states), len(states)))
    return U @ Y.T @ H @ Y @ U

# Defining a base factory for functions with extra bits attached, like 
# retrieving "extra data", storing/computing the number of optimisation 
# parameters, etc.
class EnergyFunction(metaclass=ABCMeta):
    registry = {}
    
    def __init__(self, *args, **kwargs):
        # Data such as numbers of parameters, or the specifics of the 
        # optimisation, can be set here.
        pass
    
    
    def __init_subclass__(cls, registry_key, override=False, **kwargs):
        # Automatically registers subclasses in EnergyFunction.registry
        super().__init_subclass__(**kwargs)
        if registry_key not in cls.registry.keys():
            cls.registry[registry_key] = cls
        elif override:
            cls.registry[registry_key] = cls
        else:
            raise RuntimeError(f"Key conflict on {cls.__qualname__}: {registry_key}")
    
    
    @abstractproperty
    def param_length(self):
        # The number of optimisation parameters, 
        # necessary to know before we optimise over the function.
        pass
    
    
    @abstractmethod
    def __call__(self, params, states):
        # The optimisation procedure
        pass
    
    
    @abstractmethod
    def extra_data(self, **kwargs):
        # A function to produce a (serialisable) dictionary that will be
        # bundled with the optimisation result.
        pass


class OneParamHylleraas(EnergyFunction, registry_key="ritz_upper"):
    def __init__(self, basis, *args, eigenvalue=0, **kwargs):
        assert eigenvalue < len(basis)
        self.__eigenvalue = eigenvalue
    

    @property
    def param_length(self):
        return 1
    

    @property
    def eigenvalue(self):
        return self.__eigenvalue
    

    def __call__(self, params, states):
        energies = np.sort(np.linalg.eigvals(P(states, params[0])))
        return energies[self.eigenvalue]
    

    def extra_data(self, minimize_result=None, basis=[], epsilon=-1,
                   **kwargs):
        if minimize_result is None:
            raise RuntimeError("No optimisation result provided.")
        else:
            energies, eigenvectors = np.linalg.eig(P(basis, 
                                                   minimize_result.x[0]))
            # Let's see if our variance calculation produces sane results.
            # Spoiler alert: it does not(?)
            
            # First, we need to construct the eigenstates in the correct energy
            # order. To do this, we need to find the permutation that sorts
            # the eigenvalues...
            energy_indices = np.argsort(energies)
            # ^ Thank GOD that there's a numpy function for this...
            # while we're at it, sort the eigenvalues.
            energies_sorted = energies[energy_indices]
            # apply the permutation to the array of eigenvectors, 
            eigenvectors_sorted = eigenvectors[:, energy_indices]
            # and sum!
            # How do we know which index corresponds to what basis?
            # The construction of the matrix used in P uses the ordering of
            # basis_states. We *should* be able to use that ordering.
            eigenstates = [functools.reduce(op.add, 
                            (coeff*state for coeff, state in zip(v, basis))
                           )
                           for v in eigenvectors_sorted.T]
            # I *really* hope this works.
            # Wait, don't we need to apply a transformation to the eigenvectors
            # first?
            
            # Then it's a question of iterating over the eigenstates with
            # our predefined variance function and the Hamiltonian.
            variances = [algebra.est_variance(algebra.H, state, eigenstates,
                                              energy, minimize_result.x[0])
                         for state, energy in zip(eigenstates, energies_sorted)]
            pm_sum = np.atleast_2d(
                    epsilon + sum(variance/(eigenvalue-epsilon)
                                  for variance, eigenvalue in zip(variances, 
                                                                  energies_sorted)))
            # ^ pm is from Pollak and Martinazzo
            
            pm_eigenenergies = np.diagflat(energies_sorted)
            pm_variances = np.atleast_2d(variances)
            pm_matrix = np.block([[pm_eigenenergies, pm_variances.T],
                                  [pm_variances, pm_sum]])
            pm_bounds = np.sort(np.linalg.eigvals(pm_matrix))
            return {"eigenenergies": energies_sorted,
                    "eigenvectors": eigenvectors_sorted,
                    "approx_variances": variances,
                    "pm_bounds": pm_bounds}



#
# JSON serialisation utilities.
#
def simple_serialiser(obj):
    if type(obj) == np.ndarray:
        return obj.tolist()
    #elif type(obj) == RootResults:
    #    print(obj)
    #    return dict(obj)
    else:
        raise TypeError


def json_dump_to_files(filenames, obj, /, mode="x"):
    for output_file in filenames:
        try:
            outfile = open(output_file, mode)
        except OSError as err:
            print(f"Failed to write to {output_file}: {err}",
                  file=sys.stderr)
            continue
        with outfile:
            json.dump(obj, outfile, indent="\t",
                      default=simple_serialiser)