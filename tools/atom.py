#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Module for putting atoms together from basic building blocks.
Useful for util so information about the system can easily get where it's
needed.

Created on Tue Nov  2 11:50:16 2021

@author: william
"""

class Nucleus():
    """Defines a nucleus for an atom."""
    pass

class Atom():
    """Defines an atom using a nucleus and surrounding charged particles."""
    pass