#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 24 22:44:45 2022

@author: william
"""

import itertools
#from sys import maxsize as BIGNUMBER
import unittest as ut

import numpy as np
import vegas as ve

from tools import algebra
from tools import util
from . import testdata

integ_region = np.array([[-60, 60]]*6)
# It helps when you integrate in all... 64-tants of the space, doofus!
integratinate = ve.Integrator(integ_region)

class IntegratorTest(ut.TestCase):

    def test_inner_product(self):
        ls_to_test = np.array([1.]) # np.linspace(0.1, 5.0, 10)
        states_to_test = itertools.chain(testdata.h_states_product(2), 
                                         testdata.good_states_product(2))
        param_prod = itertools.product(ls_to_test, states_to_test)
        for l, (state_1, state_2) in param_prod:
            state_1_f, state_2_f = state_1.get_func_form(l), state_2.get_func_form(l)
            @ve.batchintegrand
            def f(x):
                a = state_1_f(x)
                b = state_2_f(x)
                return a.conjugate() * b
            #print(state_1, state_2, l)
            discard = integratinate(f, nitn=50, neval=500000)
            integrand = integratinate(f, nitn=50, neval=500000)
            direct_inner_product = algebra.inner_product(state_1, state_2, L=l)
            # might be worth extracting the J, K, and M values too...
            msg = f"{state_1} {state_2} {l}; Result: {integrand}; (Dir/I): {direct_inner_product/integrand.mean}"
            with self.subTest(message=msg):
                self.assertAlmostEqual(integrand.mean, direct_inner_product,
                                       places=0)