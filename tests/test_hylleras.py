#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 14 19:52:59 2021

@author: william
"""

#__package__ = "project"  # Dark import magic
#print(f"{__package__}.{__name__}")

import unittest as ut

import numpy as np

from tools import algebra
from . import testdata


class HyllerasStateTests(ut.TestCase):

    def verify_state(self, state: algebra.HyllerasState,
                     sig: list, array: np.array):
        with self.subTest(name="Signature verification"):
            self.assertEqual(state._vecs, sig,
                             msg=f"{state._vecs} != {sig}")

        with self.subTest(name="Array verification"):
            self.assertTrue((state.array == array).all(),
                            msg=f"{state.array} != {array}")

    def test_init(self):
        vecs = [(3, 4, 5), (1, 2, 3)]
        arr = np.array([1, 0])
        foo = algebra.HyllerasState(vectors=vecs, array=arr)
        self.verify_state(foo, vecs, arr)

    def test_from_dict(self):
        foo = algebra.HyllerasState.from_dict(vectors=[(1, 1, 1),
                                                       (3, 4, 5),
                                                       (1, 1, 1)],
                                              dictionary={(1, 1, 1): 1,
                                                          (3, 4, 5): 2})
        self.verify_state(foo, [(1, 1, 1), (3, 4, 5)], np.array([1, 2]))

    def test_add_matched(self):
        foo = testdata.good_states[4] + testdata.good_states[4]
        self.verify_state(foo, [(0, 0, 0), (1, 1, 0), (3, 4, 5), (12, 4, 4)],
                          np.array([0, 0, 0, 0]))
        bar = testdata.good_states[0] + testdata.good_states[1]
        self.verify_state(bar, [(0, 0, 0), (1, 1, 0), (3, 4, 5), (12, 4, 4)],
                          np.array([1, 1, 0, 0]))

    def test_add_mismatched(self):
        exp_signature = [(0, 0, 0), (1, 1, 0), (3, 4, 5), (12, 4, 4), (5, 6, 7)]
        exp_signature2 = [(0, 0, 0), (5, 6, 7), (1, 1, 0), (12, 4, 4), (3, 4, 5)]

        foo = testdata.good_states[4] + testdata.good_states[5]
        spam = testdata.good_states[5] + testdata.good_states[4]
        bar = testdata.good_states[0] + testdata.good_states[3]
        eggs = testdata.good_states[3] + testdata.good_states[0]

        self.verify_state(foo, exp_signature, np.array([0, 0, 0, 0, 0]))
        self.verify_state(spam, exp_signature2, np.array([0, 0, 0, 0, 0]))
        self.verify_state(bar, exp_signature, np.array([2, 0, 0, 1, 1]))
        self.verify_state(eggs, exp_signature2, np.array([2, 1, 0, 1, 0]))

    def test_mul(self):
        initial = testdata.good_states[2]
        for z in [1, 2, 4, 0, -1, 1j, -3+2j]:
            self.verify_state(initial*z, initial._vecs, z*np.array([1, 1, 0, 0]))
            self.verify_state(z*initial, initial._vecs, z*np.array([1, 1, 0, 0]))
